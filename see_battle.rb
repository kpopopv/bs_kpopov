#!/usr/bin/env ruby

module Common
  def generate_table
    Array.new(10) { Array.new(10,-1) }
  end
end

class Map
  include Common

  def initialize
    #Создаём матрицу, клетки по умолчанию имеют значение -1
    @map = generate_table
    @layer = generate_table
  end

  def layer
    @layer
  end

  def set_ships
    #Проход по видам кораблей (4-х палубники, 3-х палубники...)
    3.downto(0) do |length|
      #проход по количествам  таких кораблей
      0.upto(3-length) do
        #Перебор всех допустимых вариантов
        begin
          #Генерация случайной изначальной позиции
          x = rand(10)
          y = rand(10)
          #Выбор случайного направления
          kx = rand(2)
          kx == 0 ? ky = 1 : ky = 0
          is_available = true
          #Вариант не подходит, если не доступна одна из клеток
          0.upto(length) do |i|
            unless is_cell_freedom(x+kx*i,y+ky*i)
              is_available = false
              break
            end
          end
          #Если всё нормально устанавливаем корабль
          if is_available
            0.upto(length) do |i|
              @map[x+kx*i][y+ky*i] = 0
            end
          end
        end while !is_available
      end
    end
  end

  def is_cell_freedom x,y
    #Если целевая ячейка пуста, то проверяем соседние, если нет, сразу ложь
    if (x >= 0) and (x <= 9) and (y >= 0) and (y <= 9) and (@map[x][y] == -1)
      d = [[0,1],[1,0],[0,-1],[-1,0],[1,1],[-1,1],[1,-1],[-1,-1]]
      0.upto(7) do |i|
        dx = x + d[i][0]
        dy = y + d[i][1]
        #Проверка соседних с целевой ячейкой
        if (dx >= 0) and (dx <= 9) and (dy >= 0) and (dy <= 9) and (@map[dx][dy] == 0)
          return false
        end
      end
      #Если все свободные, то истина
      true
    else
      false
    end
  end

  def render
    #Первая строка
    print ' '
    #Изначальный символ для генерации колонок столбца
    th = '@'
    0.upto(9) do
      print th.next!
    end
    puts
    @map.each_with_index do |row,row_index|
      row.each_with_index  do |cell,cell_index|
        #Первый столбец
        print row_index if cell_index == 0
        if block_given?
          #Регулярный вывод задаётся в классе приложения
          yield cell,row_index,cell_index
        else
          #Вывод позиций
          if @layer[row_index][cell_index] == 1
            cell == 0 ? p = 'x' : p = 'o'
          else
            cell == 0 ? p = 's' : p = '.'
          end
          print p
        end
      end
      puts
    end
  end

  def hit_count
    count = 0
    @map.each_with_index do |row,row_index|
      row.each_with_index  do |cell,cell_index|
        count+=1 if cell == 0 and @layer[row_index][cell_index] == 1
      end
    end
    count
  end

  def target_count
    @map.map { |item| item.select { |item| item == 0 }.size }.inject(:+)
  end

  def mark_cell x,y
    @layer[y][x] = 1
  end

end

class App

  def initialize
    @is_surrender = false
    @map = Map.new
  end

  def run
    @map.set_ships
    #puts 'Show positions for debug!'
    #@map.render
    puts 'Game started!'

    #Повторять до тех пор, пока число клеток с кораблями превышает число попаданий
    while @map.hit_count < @map.target_count  and !@is_surrender
      #Регулярный вывод сетки
      if @is_surrender == false
      @map.render do |cell,row_index,cell_index|
        if @map.layer[row_index][cell_index] == 1
          print 'x' if cell == 0
          print 'o' if cell == -1
        else
          print '.'
        end
      end
      end

      #Получение координаты
      puts 'Enter the coordinate of the ship (not case sensitive):'
      input = gets
      regexp = Regexp.new(/^[a-jA-J]\d$/)
      if regexp.match(input)
        x,y = parse_coordinate input
        @map.mark_cell x,y
      else
        if input.chomp == 'surrender'
          puts 'What a shame...'
          @is_surrender = true
        else
          puts 'Incorrect command, please repeat!'
        end
      end
    end

    if !@is_surrender
      puts 'You win!'
    end

    @map.render
    puts 'Game finished!'
  end

  def parse_coordinate input
    [input[0].downcase.bytes.to_a[0].to_i - 97,input[1].to_i]
  end

end

App.new.run